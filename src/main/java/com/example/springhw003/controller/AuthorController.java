package com.example.springhw003.controller;

import com.example.springhw003.expection.IdNotFoundException;
import com.example.springhw003.model.entity.Author;
import com.example.springhw003.model.request.AuthorRequest;
import com.example.springhw003.model.response.ReponseRequest;
import com.example.springhw003.service.serviceImp.AuthorServiceImp;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/authors")
@AllArgsConstructor
public class AuthorController {
    private final AuthorServiceImp authorServiceImp;
    @GetMapping("")
    @Operation(summary = "Get all List of Authors")
    public ResponseEntity<?> getAllAuthors(){
        ReponseRequest<?> response = ReponseRequest.<List<Author>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Successfully add author")
                .payload(authorServiceImp.getAllAuthors())
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get Author by Id")
    public ResponseEntity<?> getAuthorById(@PathVariable("id") Integer authorId){
        ReponseRequest<?> response = null;
        if(authorServiceImp.getAuthorById(authorId) != null)
        {
           response= ReponseRequest.<Author>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully add author")
                    .payload(authorServiceImp.getAuthorById(authorId))
                    .build();
            return ResponseEntity.ok(response);
        }
       else throw new IdNotFoundException("Author ID is not found");
    }
    @PostMapping("/add")
    @Operation(summary = "Insert New Author")
    public ResponseEntity<?> insertNewAuthor(@Valid  @RequestBody AuthorRequest authorRequest){
        Integer getAuthorId = authorServiceImp.insertNewAuthor(authorRequest);
        System.out.println("InterGer " + getAuthorId );
        ReponseRequest<?> response = ReponseRequest.<Author>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Insert Author Successfully")
                .payload(authorServiceImp.getAuthorById(getAuthorId))
                .build();
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete Author By Id")
    public ResponseEntity<?> deleteAuthorById(@PathVariable("id") Integer authorId){
        ReponseRequest<?> response = null;
        if(authorServiceImp.deleteAuthorById(authorId)){
            response = ReponseRequest.<Author>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Delete ID :"+authorId+" Author Successfully")
                    .build();
        return ResponseEntity.ok(response);
        }
        else throw new IdNotFoundException("Author ID "+authorId + " is not found");
    }

@PutMapping("/update/{id}")
    @Operation(summary = "Update Authors By Id")
    public ResponseEntity<?>updateAuthorById (@RequestBody AuthorRequest authorRequest, @PathVariable("id") Integer authorId){
    Integer getAuthorIdAfterUpdate =authorServiceImp.updateAuthorById(authorRequest,authorId);
    ReponseRequest<?> response = null;
    if(getAuthorIdAfterUpdate != null)
    {
        response = ReponseRequest.<Author>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Update Author SuccessFully")
                .payload(authorServiceImp.getAuthorById(getAuthorIdAfterUpdate))
                .build();
        return ResponseEntity.ok(response);
    }
    else throw new IdNotFoundException("Author ID "+authorId + " is not found");
}

}
