package com.example.springhw003.controller;

import com.example.springhw003.expection.IdNotFoundException;
import com.example.springhw003.model.entity.Book;
import com.example.springhw003.model.entity.Category;
import com.example.springhw003.model.request.CategoryRequest;
import com.example.springhw003.model.response.ReponseRequest;
import com.example.springhw003.service.serviceImp.CategoryServiceImp;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/categories")
public class CategoryController {
    private final CategoryServiceImp categoryServiceImp;
    @GetMapping("")
    @Operation(summary = "Get ALL Category")
    public ResponseEntity<?> getAllCategory()
    {
        ReponseRequest<List<Category>> reponseRequest = ReponseRequest.<List<Category>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Success")
                .payload(categoryServiceImp.getAllCategory())
                .build();
        return ResponseEntity.ok(reponseRequest);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get Category By Id")
    public ResponseEntity<?> getCategoryById(@PathVariable("id") Integer cateId){
        if( categoryServiceImp.getCategoryById(cateId) !=null)
        {
            ReponseRequest<?> reponseRequest = ReponseRequest.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Get By ID SuccessFully")
                    .payload(categoryServiceImp.getCategoryById(cateId))
                    .build();
            return ResponseEntity.ok(reponseRequest);
        }else  throw new IdNotFoundException("Cant not Get category Id "+cateId+ " is not found");
    }

    @PostMapping("/add-new-Category")
    @Operation(summary = "Insert new category")
    public ResponseEntity<?> insertNewCategory(@RequestBody CategoryRequest categoryRequest){
        if(categoryServiceImp.addNewCategory(categoryRequest) != null) {
            ReponseRequest<?> reponseRequest = ReponseRequest.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .message("Insert SuccessFully")
                    .status(200)
                    .build();
            return ResponseEntity.ok(reponseRequest);
        }
        return null;
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete Category By Id")
    public ResponseEntity<?> deleteCategory(@PathVariable("id") Integer cateId){
        if(categoryServiceImp.deleteCategory(cateId))
        {
            ReponseRequest<?> reponseRequest = ReponseRequest.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .message("Delete SuccessFully")
                    .status(200)
                    .build();
        return ResponseEntity.ok(reponseRequest);
        }else throw new IdNotFoundException("Can not Delete category ID "+ cateId + " is not found");
    }

    @PutMapping("/update/{id}")
    @Operation(summary = "Update Category By ID")
    public ResponseEntity<?> updateCategoryById(@RequestBody CategoryRequest categoryRequest,@PathVariable("id") Integer cateId){
        Integer storeCateId = categoryServiceImp.updateCategory(categoryRequest,cateId);
        if(storeCateId!=null)
        {
            ReponseRequest<?> reponseRequest = ReponseRequest.<Category>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Update SuccessFully")
                    .payload(categoryServiceImp.getCategoryById(storeCateId))
                    .build();
            return ResponseEntity.ok(reponseRequest);
        }else  throw new IdNotFoundException("Cant not Get category Id "+cateId+ " is not found");
    }
}
