package com.example.springhw003.controller;

import com.example.springhw003.expection.IdNotFoundException;
import com.example.springhw003.model.entity.Book;
import com.example.springhw003.model.entity.Category;
import com.example.springhw003.model.request.BookRequest;
import com.example.springhw003.model.response.ReponseRequest;
import com.example.springhw003.service.serviceImp.BookServiceImp;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("books")
public class BookController {
    private final BookServiceImp bookServiceImp;

    @GetMapping("")
    @Operation(summary = "Get Books List")
    public ResponseEntity<?> getAllBooks(){
        ReponseRequest<?> reponseRequest = ReponseRequest.<List<Book>>builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(200)
                .message("Successfully fetched books")
                .payload(bookServiceImp.getAllBooks())
                .build();
        return ResponseEntity.ok(reponseRequest);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get Book By ID")
    public ResponseEntity<?> getBookById (@PathVariable("id") Integer bookId){
        if( bookServiceImp.getBookById(bookId)!=null)
        {
            ReponseRequest<?> reponseRequest = ReponseRequest.<Book>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Successfully Get By ID Book")
                    .payload(bookServiceImp.getBookById(bookId))
                    .build();
            return ResponseEntity.ok(reponseRequest);
        }else throw new IdNotFoundException("Can not Update book ID "+bookId+" is not found");
    }

    @PostMapping("/add")
    @Operation(summary = "Successfully added Book")
    public ResponseEntity<?> insertNewBook(@RequestBody BookRequest bookRequest){
        Integer storeBookId = bookServiceImp.insertBook(bookRequest);
        return ResponseEntity.ok(bookServiceImp.getBookById(storeBookId));
    }

    @PutMapping("/update/{id}")
    @Operation(summary = "Update Book By Id ")
    public ResponseEntity<?> updateBookById(@RequestBody BookRequest bookRequest, @PathVariable("id") Integer bookId){
        Integer storeBookId = bookServiceImp.updateBookById(bookRequest,bookId);
        if(storeBookId!=null)
        {
            ReponseRequest<?> reponseRequest = ReponseRequest.<Book>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .status(200)
                    .message("Update SuccessFully")
                    .payload(bookServiceImp.getBookById(storeBookId))
                    .build();
            return ResponseEntity.ok(reponseRequest);
        }else throw new IdNotFoundException("Can not Update book ID "+bookId+" is not found");
    }

    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete Book By Id")
    public ResponseEntity<?> deleteById(@PathVariable("id") Integer bookId){
        ReponseRequest<?> response = null;
        if(bookServiceImp.deleteBookById(bookId)){
            response = ReponseRequest.<Book>builder()
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .message("Delete Success")
                    .status(200)
                    .build();
            return ResponseEntity.ok(response);
        }
        else throw new IdNotFoundException("can not delete Book Id "+bookId+ " is not found");
    }
}

