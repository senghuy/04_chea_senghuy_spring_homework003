package com.example.springhw003.repository;

import com.example.springhw003.model.entity.Book;
import com.example.springhw003.model.entity.Category;
import com.example.springhw003.model.request.CategoryRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {
    @Select("SELECT * FROM categories ORDER BY category_id ASC")
    @Results(
            id ="categoryMapper",
            value = {
                    @Result(property = "id" , column = "category_id"),
                    @Result(property = "name", column = "category_name"),
            }
    )
    List<Category> getAllCategory();


    @Select("""
            INSERT INTO categories (category_name) 
            VALUES (#{req.category_name}) RETURNING category_id
            """)
    Integer insertNewCategory(@Param("req") CategoryRequest categoryRequest);

    @Delete("""
            DELETE FROM categories WHERE category_id = #{cateId}
            """)
    boolean deleteCategory(Integer cateId);

    @Select("""
            UPDATE categories SET category_name = #{req.category_name}
            WHERE category_id = #{cateId} RETURNING category_id
            """)
    Integer updateCategory(@Param("req") CategoryRequest CategoryRequest, Integer cateId);

    @Select("""
            SELECT * FROM categories WHERE category_id = #{cateId}
            """)
    @ResultMap("categoryMapper")
    Category getCategoryById(Integer cateId);

    @Select("""
            SELECT c.category_id, c.category_name FROM book_details bk
            INNER JOIN categories c on c.category_id = bk.category_id
            WHERE book_id = #{bookId}
            """)
    @ResultMap("categoryMapper")
    List<Book> findBookById(Integer bookId);
}
