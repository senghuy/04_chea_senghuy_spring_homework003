package com.example.springhw003.repository;

import com.example.springhw003.model.entity.Book;
import com.example.springhw003.model.request.BookRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookRepository {


    @Select("SELECT * FROM books ORDER BY book_id ASC")
    @Results(
            id ="bookMapper",
            value= {
                    @Result(property = "id",column = "book_id"),
                    @Result(property = "publishedDate",column = "published_date"),
                    @Result(property = "author", column = "author_id",
                            one = @One (select = "com.example.springhw003.repository.AuthorRepository.getAuthorById")),
                    @Result(property = "categoryId", column = "book_id",
                            many = @Many (select = "com.example.springhw003.repository.CategoryRepository.findBookById"))
            }
    )
    List<Book> getAllBooks();
    @Select("""
            SELECT * FROM books WHERE book_id = #{bookId}
            """)
    @ResultMap("bookMapper")
    Book getBookById(Integer bookId);
    @Select("""
          INSERT INTO books (title, published_date, author_id) VALUES
          (#{req.title}, #{req.publishedDate}, #{req.author} ) 
          RETURNING book_id
            """)
    @Result(property = "id",column = "book_id")
    Integer savedNewBook (@Param("req")BookRequest bookRequest);
//
    @Select("""
            INSERT INTO book_details (book_id, category_id)
            VALUES (#{bookId}, #{cateId})
            """)
    Integer savedCategoryByBookDetial(Integer bookId, Integer cateId);
//
    @Select("""
            UPDATE books SET title = #{req.title} ,published_date = #{req.publishedDate}
            , author_id = #{req.author}
            WHERE book_id = #{bookId} RETURNING book_id
            """)
    Integer updateBookById(@Param("req")BookRequest bookRequest, Integer bookId);

    @Select("""
            DELETE FROM book_details WHERE book_id = #{bookId}
            """)
    Integer deleteBookIdInBookDetails(Integer bookId);

    @Delete("""
           DELETE FROM books WHERE book_id = #{bookId}
            """)
    boolean deleteBookById(Integer bookId);
}
