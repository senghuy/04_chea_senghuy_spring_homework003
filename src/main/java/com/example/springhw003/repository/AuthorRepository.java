package com.example.springhw003.repository;

import com.example.springhw003.model.entity.Author;
import com.example.springhw003.model.request.AuthorRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface AuthorRepository {

    @Select("SELECT * FROM authors ORDER BY author_id ASC")
    @Results(
            id ="authorMapper",
            value = {
                    @Result(property = "id" , column = "author_id"),
                    @Result(property = "name", column = "author_name"),
            }
    )
    List<Author> getAllAuthors();

    @Select("SELECT * FROM authors WHERE author_id = #{authorId}")
    @ResultMap("authorMapper")
    Author getAuthorById(Integer authorId);
@Select("""
        INSERT INTO authors (author_name,gender) 
        VALUES (#{req.name} , #{req.gender}) RETURNING author_id
        """)
    Integer addNewAuthor(@Param("req")AuthorRequest authorRequest);
    @Delete("DELETE FROM authors WHERE author_id = #{authorId}")
    boolean deleteAuthorById(Integer auInteger);

    @Select("""
            UPDATE authors SET author_name = #{req.name},
            gender = #{req.gender} WHERE author_id = #{authorId} RETURNING author_id
            """)

    Integer updateAuthor(@Param("req")AuthorRequest authorRequest, Integer authorId);
}
