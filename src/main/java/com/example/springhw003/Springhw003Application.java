package com.example.springhw003;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springhw003Application {

	public static void main(String[] args) {
		SpringApplication.run(Springhw003Application.class, args);
	}

}
