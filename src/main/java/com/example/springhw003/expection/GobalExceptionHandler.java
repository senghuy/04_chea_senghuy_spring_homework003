package com.example.springhw003.expection;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.URI;
import java.time.Instant;

@RestControllerAdvice
public class GobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(IdNotFoundException.class)
    ProblemDetail handlerIdNotFoundException(IdNotFoundException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, e.getMessage());
        problemDetail.setTitle(e.getMessage());
        problemDetail.setDetail("not found");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/authors/not-found"));
        return problemDetail;
    }
    @ExceptionHandler(IsNullException.class)
    ProblemDetail handlerNullExcepetion(IsNullException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setTitle(e.getMessage());
        problemDetail.setDetail("data contain null");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/authors/bad-request"));
        return problemDetail;
    }
    @ExceptionHandler(GenderException.class)
    ProblemDetail handlerGenderExcepetion(GenderException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setTitle(e.getMessage());
        problemDetail.setDetail("Gender is invalid");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/authors/bad-request"));
        return problemDetail;
    }

    @ExceptionHandler(IsNotNumberException.class)
    ProblemDetail handlerNumberExcepetion(IsNotNumberException e){
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, e.getMessage());
        problemDetail.setTitle(e.getMessage());
        problemDetail.setDetail("Input in number");
        problemDetail.setProperty("timestamp", Instant.now());
        problemDetail.setType(URI.create("http://localhost:8080/api/v1/authors/bad-request"));
        return problemDetail;
    }
}
