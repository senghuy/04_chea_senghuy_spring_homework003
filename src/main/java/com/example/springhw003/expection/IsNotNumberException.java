package com.example.springhw003.expection;

public class IsNotNumberException extends RuntimeException{
    public IsNotNumberException(String message) {
        super(message);
    }
}
