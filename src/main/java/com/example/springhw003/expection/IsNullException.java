package com.example.springhw003.expection;

public class IsNullException extends RuntimeException{
    public IsNullException(String message) {
        super(message);
    }
}
