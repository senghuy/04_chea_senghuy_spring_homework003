package com.example.springhw003.expection;

public class IdNotFoundException extends RuntimeException{
    public IdNotFoundException(Integer id){
        super("");
    }

    public IdNotFoundException(String message) {
        super(message);
    }
}
