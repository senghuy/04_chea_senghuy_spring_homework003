package com.example.springhw003.service;


import com.example.springhw003.model.entity.Book;
import com.example.springhw003.model.request.BookRequest;

import java.util.List;

public interface BookService {

    List<Book> getAllBooks();
    Book getBookById(Integer bookId);
    Integer insertBook(BookRequest bookRequest);
    Integer updateBookById(BookRequest bookRequest, Integer bookId);
    boolean deleteBookById(Integer bookId);
}
