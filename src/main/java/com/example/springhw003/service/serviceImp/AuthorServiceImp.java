package com.example.springhw003.service.serviceImp;

import com.example.springhw003.expection.GenderException;
import com.example.springhw003.expection.IsNullException;
import com.example.springhw003.model.entity.Author;
import com.example.springhw003.model.request.AuthorRequest;
import com.example.springhw003.repository.AuthorRepository;
import com.example.springhw003.service.AuthorService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AuthorServiceImp implements AuthorService {
    private final AuthorRepository authorRepository;
    @Override
    public List<Author> getAllAuthors() {
        return authorRepository.getAllAuthors();
    }

    @Override
    public Author getAuthorById(Integer authorId) {
        return authorRepository.getAuthorById(authorId);
    }

    @Override
    public Integer insertNewAuthor(AuthorRequest authorRequest) {
        String storeGender = authorRequest.getGender().toLowerCase();
        String authorName = authorRequest.getName().toLowerCase();
       if(authorName.isBlank())
       {
           throw new IsNullException("Input invalidate");
       }
       else {
           if(storeGender.equals("male") || storeGender.equals("female")) {
               return authorRepository.addNewAuthor(authorRequest);
           }else throw new GenderException("Author's gender isn't right");
       }
    }


    @Override
    public boolean deleteAuthorById(Integer authorId) {
        return authorRepository.deleteAuthorById(authorId);
    }

    @Override
    public Integer updateAuthorById(AuthorRequest authorRequest, Integer authorId) {
        String storeGender = authorRequest.getGender().toLowerCase();
        String authorName = authorRequest.getName().toLowerCase();
        if(authorName.isBlank())
        {
            throw new IsNullException("Input invalidate");
        }
        else {
            if(storeGender.equals("male") || storeGender.equals("female")) {
                return authorRepository.updateAuthor(authorRequest,authorId);
            }else throw new GenderException("Author's gender isn't right");
        }
    }
}
