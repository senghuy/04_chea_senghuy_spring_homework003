package com.example.springhw003.service.serviceImp;

import com.example.springhw003.expection.IsNullException;
import com.example.springhw003.model.entity.Category;
import com.example.springhw003.model.request.CategoryRequest;
import com.example.springhw003.repository.CategoryRepository;
import com.example.springhw003.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class CategoryServiceImp implements CategoryService {
    private final CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategory() {
        return categoryRepository.getAllCategory();
    }

    @Override
    public Category getCategoryById(Integer cateId) {
        return categoryRepository.getCategoryById(cateId);
    }

    @Override
    public Integer addNewCategory(CategoryRequest categoryRequest) {
        String cateName = categoryRequest.getCategory_name().toLowerCase();
        if (cateName.isBlank()){
          throw new IsNullException("Category name is empty");
        }
        else {
        return categoryRepository.insertNewCategory(categoryRequest);
        }
    }

    @Override
    public boolean deleteCategory(Integer cateId) {
        return categoryRepository.deleteCategory(cateId);
    }

    @Override
    public Integer updateCategory(CategoryRequest categoryRequest, Integer cateId) {
        String cateName = categoryRequest.getCategory_name().toLowerCase();
        if(cateName.isBlank()) {
            throw new IsNullException("Category name is empty");
        }
        else {
            return categoryRepository.updateCategory(categoryRequest, cateId);
        }
    }
}
