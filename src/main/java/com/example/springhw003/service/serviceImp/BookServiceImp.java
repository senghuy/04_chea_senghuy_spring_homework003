package com.example.springhw003.service.serviceImp;

import com.example.springhw003.expection.IsNullException;
import com.example.springhw003.expection.IsNotNumberException;
import com.example.springhw003.model.entity.Book;
import com.example.springhw003.model.request.BookRequest;
import com.example.springhw003.repository.BookRepository;
import com.example.springhw003.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class BookServiceImp implements BookService {
    private final BookRepository bookRepository;

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.getAllBooks();
    }

    @Override
    public Book getBookById(Integer bookId) {
        return bookRepository.getBookById(bookId);
    }

    @Override
    public Integer insertBook(BookRequest bookRequest) {
        String bookName = bookRequest.getTitle().toLowerCase();
        String  authorId = bookRequest.getAuthor().toString();
        String regex = "[1-9]+";
        System.out.println(bookRequest.getCategoryId().toString());
        if(bookName.isBlank())
        {
            throw new IsNullException("Book title is empty");
        }else if(authorId.matches(regex))
        {
            Integer storeId = bookRepository.savedNewBook(bookRequest);
            for (Integer cateId : bookRequest.getCategoryId())
            {
                if(bookRequest.getCategoryId().toString().matches(regex)) {
                    bookRepository.savedCategoryByBookDetial(storeId, cateId);
                }
                else {
                    throw new IsNotNumberException("Category ID is not a number ");
                }
            }
            return storeId;
        }
        return null;
    }

    @Override
    public Integer updateBookById(BookRequest bookRequest, Integer bookId) {
        String bookName = bookRequest.getTitle().toLowerCase();
        String  authorId = bookRequest.getAuthor().toString();
        List<Integer> categoryId = bookRequest.getCategoryId();
        String regex = "[1-9]+";
        if(bookName.isBlank())
        {
            throw new IsNullException("Book title is empty");
        }else if(authorId.matches(regex))
        {
            Integer storebookId = bookRepository.updateBookById(bookRequest,bookId);
            bookRepository.deleteBookIdInBookDetails(storebookId);
            for (Integer cateId : bookRequest.getCategoryId())
            {
                if(cateId.toString().matches(regex)) {
                    bookRepository.savedCategoryByBookDetial(bookId,cateId);
                }
                else {
                    throw new IsNotNumberException("Category ID is not a number");
                }
            }
            return storebookId;
        }
        return null;
    }

    @Override
    public boolean deleteBookById(Integer bookId) {
        return bookRepository.deleteBookById(bookId);
    }
}
