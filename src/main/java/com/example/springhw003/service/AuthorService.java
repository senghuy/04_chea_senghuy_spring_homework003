package com.example.springhw003.service;

import com.example.springhw003.model.entity.Author;
import com.example.springhw003.model.request.AuthorRequest;

import java.util.List;

public interface AuthorService {
    List<Author> getAllAuthors();
    Author getAuthorById(Integer authorId);
    Integer insertNewAuthor(AuthorRequest authorRequest);
    boolean deleteAuthorById(Integer authorId);
    Integer updateAuthorById(AuthorRequest authorRequest, Integer authorId);
}
