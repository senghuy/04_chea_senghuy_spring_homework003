package com.example.springhw003.service;

import com.example.springhw003.model.entity.Category;
import com.example.springhw003.model.request.CategoryRequest;

import java.util.List;

public interface CategoryService {
    List<Category> getAllCategory();
    Category getCategoryById(Integer cateId);
    Integer addNewCategory(CategoryRequest categoryName);
    boolean deleteCategory(Integer cateId);
    Integer updateCategory(CategoryRequest cateName, Integer cateId);
}
