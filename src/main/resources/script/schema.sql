CREATE TABLE authors(
    author_id SERIAL PRIMARY KEY ,
    author_name varchar(50) not null ,
    gender varchar(50) not null
);

CREATE TABLE categories (
    category_id SERIAL PRIMARY KEY ,
    category_name varchar(255) not null
);

CREATE TABLE books (
    book_id SERIAL PRIMARY KEY ,
    title varchar(255) not null ,
    published_date timestamp  not null ,
    author_id int REFERENCES authors(author_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE book_details(
    id SERIAL PRIMARY KEY ,
    book_id int REFERENCES books(book_id) ON DELETE CASCADE ON UPDATE CASCADE ,
    category_id int REFERENCES categories(category_id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO authors (author_name, gender) VALUES ('Meng','Male'),
                                                 ('Ling','Female'),
                                                 ('Heng','Male'),
                                                 ('Nary','Female'),
                                                 ('Rin','Female');

INSERT INTO categories (category_name) VALUES ('Fantasy'),
                                              ('Education'),
                                              ('Crime'),
                                              ('Programming'),
                                              ('Mastery');


INSERT INTO books (title, published_date, author_id) VALUES ('Java','03-01-2022',1),
                                                            ('C++','02-12-2022',2),
                                                            ('C','02-11-2021',2),
                                                            ('HTML','01-01-2000',3),
                                                            ('CSS','02-03-2018',4),
                                                            ('A+','02,02,2019',5),
                                                            ('JavaScript','01-01-2005',5);
INSERT INTO book_details(book_id, category_id) VALUES (1,2),(1,4),(2,1),(2,4),(3,2),(4,2),(5,2),(6,2),(7,2);

SELECT * FROM authors ;

SELECT * FROM authors WHERE author_id = 2;

UPDATE authors SET author_name = 'na' , gender = 'Female' WHERE author_id = 1 RETURNING author_id;


INSERT INTO authors (author_name, gender)
             VALUES ('nit','Female') RETURNING author_id;



SELECT c.category_id, c.category_name FROM book_details bk
INNER JOIN categories c on c.category_id = bk.category_id
WHERE book_id = 1;



INSERT INTO books (title, published_date, author_id)
VALUES ('Happy','02-14-2012',6)
RETURNING book_id;

INSERT INTO books (title, published_date, author_id) VALUES
    ('Happy','02-14-2012',6 ) RETURNING book_id;
INSERT INTO book_details (book_id, category_id)
VALUES (15, 1);

SELECT * FROM categories WHERE category_id = 10;

select * from books where book_id = 8;

SELECT * from authors;
UPDATE authors SET author_name = 'AB'
                 , gender = 'M' WHERE author_id = 29;

select * from books ORDER BY book_id ASC ;


